package makeitmodena.sos.RecyclerView;

import android.view.View;

/**
 * Created by singh on 6/24/17.
 */

public interface OnItemClick {
    void itemClicked(View v);
}
