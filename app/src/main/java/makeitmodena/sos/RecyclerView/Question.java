package makeitmodena.sos.RecyclerView;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import makeitmodena.sos.QuestionActivity;
import makeitmodena.sos.R;

/**
 * Created by singh on 6/24/17.
 */

public class Question extends RecyclerView.Adapter<Question.ViewHolder>{

    public Context context;
    private String[][] mDataset;
    public Question(Context ctx, String[][] myDataset){
        this.context = ctx;
        this.mDataset = myDataset;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {
        public TextView mTextView;
        public LinearLayout mView;
        public OnItemClick mHolderListener;
        public ViewHolder (LinearLayout v,OnItemClick listener){
            super(v);
            mView = v;
            mHolderListener = listener;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view){
            mHolderListener.itemClicked(view);

        }
    }
    @Override
    public Question.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_question, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v, new OnItemClick() {
            @Override
            public void itemClicked(View view) {
                Log.d("CLicked","clild");
                TextView textViewTitle = (TextView) view.findViewById(R.id.questionText);
                TextView textViewBody = (TextView) view.findViewById(R.id.questionBody);
                if(textViewBody.getVisibility() == View.GONE)
                    textViewTitle.setTextColor(ContextCompat.getColor(view.getContext(),R.color.colorRed));
                else  textViewTitle.setTextColor(ContextCompat.getColor(view.getContext(),R.color.colorBlack));
                textViewBody.setVisibility((
                        textViewBody.getVisibility() == View.VISIBLE ? View.GONE:View.VISIBLE
                    ));
                ImageView expand = ((ImageView) view.findViewById(R.id.expand));
                expand.setVisibility((
                        expand.getVisibility() == View.VISIBLE ? View.GONE: View.VISIBLE ));
                ImageView unexpand = ((ImageView) view.findViewById(R.id.unexpand));
                unexpand.setVisibility((
                        unexpand.getVisibility() == View.VISIBLE ? View.GONE: View.VISIBLE ));

            }
        });
        return vh;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        ((TextView) holder.mView.findViewById(R.id.questionText)).setText(mDataset[position][0]);
        ((TextView) holder.mView.findViewById(R.id.questionBody)).setText(mDataset[position][1]);


    }
    @Override
    public int getItemCount() {
        return mDataset.length;
    }

}
