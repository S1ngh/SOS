package makeitmodena.sos;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Random;
import java.util.TimeZone;

import static android.view.MotionEvent.INVALID_POINTER_ID;

public class BlockActivity extends AppCompatActivity implements onFragmentDetached {
    private ImageView mImageView;
    private CountDownTimer thread;
    private TextView time;
    private TextView date;
    private FrameLayout touchArea;
    private LinearLayout infoArea;
    private FrameLayout parent;
    private FrameLayout parentFragment;
    private DisplayMetrics metrics;
    private float mLastTouchX;
    private float mLastTouchY;
    private int mHeight;
    private int mWidth;
    private int mActivePointerId = INVALID_POINTER_ID;
    @Override
    public void fragmentDetached(int i){
        thread.cancel();
        resetAnim();
        initWorkspace();

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block);
        parent = (FrameLayout) findViewById(R.id.parentBlock);
        parentFragment = (FrameLayout) findViewById(R.id.parentFragment);

        parent.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);

        mImageView  = (ImageView)findViewById(R.id.wallpaper);
        touchArea   = (FrameLayout) findViewById(R.id.touchArea);
        infoArea    = (LinearLayout) findViewById(R.id.infoArea);
        time = (TextView) findViewById(R.id.time);
        date = (TextView) findViewById(R.id.date);

        time.setTypeface(Typeface.createFromAsset(getAssets(),"font/Roboto-Thin.ttf"));
        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mHeight     = metrics.heightPixels;
        mWidth      = metrics.widthPixels;
        initWorkspace();

        touchArea.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) v.getLayoutParams();
                int index = event.getActionIndex();
                float x = 0;
                float y = 0;
                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_MOVE:

                        int pointerIndex = event.getPointerId(mActivePointerId);
                        x = event.getX(pointerIndex);
                        y = event.getY(pointerIndex);
                        final float dx = x - mLastTouchX;
                        final float dy = y - mLastTouchY ;
                        v.setTranslationY(dy/3);
                        infoArea.setScaleY(1 - Math.abs(dy) / 5000);
                        infoArea.setScaleX(1 - Math.abs(dy) / 5000);
                        infoArea.setAlpha(1-Math.abs(dy)/1000);
                        System.out.println(v.getTranslationY());

                        break;
                    case MotionEvent.ACTION_DOWN:
                        // Add a user's movement to the tracker.

                        pointerIndex = MotionEventCompat.getActionIndex(event);
                        x = event.getX(pointerIndex);
                        y = event.getY(pointerIndex);

                        // Remember where we started (for dragging)
                        mLastTouchX = x;
                        mLastTouchY = y;
                        // Save the ID of this pointer (for dragging)
                        mActivePointerId = event.getPointerId(pointerIndex);

                        break;
//
                    case MotionEvent.ACTION_UP:
                        if(Math.abs(v.getTranslationY()) > 130 ) {
                            System.out.println(mHeight);
                            ObjectAnimator lastTranslatorY = ObjectAnimator.ofFloat(touchArea, "TranslationY", -mHeight );
                            lastTranslatorY.setDuration(200);
                            lastTranslatorY.setInterpolator(new AccelerateInterpolator());
                            lastTranslatorY.start();
                            makeFragment();
                        }
                        else
                        resetAnim();
                        break;
                    default:

                        break;

                }
                return true;
            }
        });

    }
    public void makeFragment(){
//        Fragment callFragment =  new CallFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.animator.slide_bottom_totop, R.animator.slide_up_tobottom,R.animator.slide_bottom_totop, R.animator.slide_up_tobottom);
        transaction.replace(R.id.parentFragment, new CallFragment(),"Call");
        transaction.addToBackStack(null);
        transaction.commit();
    }
    public void resetAnim(){

        /*Alpha*/

        ObjectAnimator alpha    = ObjectAnimator.ofFloat(infoArea, "alpha", 1);
        alpha.setDuration(500);
        alpha.setInterpolator(new AccelerateInterpolator());
        /*Arrow and text sos reset*/
        ObjectAnimator TranslatorY = ObjectAnimator.ofFloat(touchArea, "TranslationY", 1);
        TranslatorY.setDuration(500);
        TranslatorY.setInterpolator(new BounceInterpolator());

        /*Scaling info area*/

        ObjectAnimator scaleX = ObjectAnimator.ofFloat(infoArea, "scaleX", 1);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(infoArea, "scaleY", 1);
        scaleX.setDuration(500);
        scaleY.setDuration(500);
        scaleX.setInterpolator(new BounceInterpolator());
        scaleY.setInterpolator(new BounceInterpolator());
        AnimatorSet scaleDown = new AnimatorSet();
        scaleDown.playTogether(scaleX,scaleY,TranslatorY,alpha);
        scaleDown.start();

    }
    public void initWorkspace(){
        InputStream ims;
        try {
            ims = getAssets().open("images/material1.jpg");
            Drawable d = Drawable.createFromStream(ims, null);
            mImageView.setImageDrawable(d);

        }catch (IOException e){
            e.printStackTrace();
        }
        Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        final int minutes = c.get(Calendar.MINUTE);
        final int hours   = c.get(Calendar.HOUR_OF_DAY);
        final int day     = c.get(Calendar.DAY_OF_MONTH);
        final int month     = c.get(Calendar.MONTH);
        final int year     = c.get(Calendar.YEAR);
        date.setText(day+"-"+month+"-"+year);

        thread = new CountDownTimer(Integer.MAX_VALUE, 1000) {

            public void onTick(long millisUntilFinished) {

                time.setText(hours+":"+minutes);

            }

            public void onFinish() {



            }
        }.start();
        // load image as Drawable

        // set image to ImageView
    }
    @Override
    protected void onResume(){
        super.onResume();
        thread.cancel();
        resetAnim();
        initWorkspace();
    }
}
