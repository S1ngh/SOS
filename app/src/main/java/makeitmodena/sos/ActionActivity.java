package makeitmodena.sos;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;

import android.app.FragmentManager;
import android.app.FragmentTransaction;

import android.content.DialogInterface;
import android.graphics.Typeface;

import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.view.MotionEventCompat;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.app.Fragment;


import com.hanks.htextview.HTextView;
import com.varunest.sparkbutton.SparkButton;
import com.varunest.sparkbutton.SparkEventListener;

import java.util.Random;

import static android.view.MotionEvent.INVALID_POINTER_ID;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class ActionActivity extends AppCompatActivity implements onFragmentDetached {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private View mContentView;
    private View mControlsView;
    private boolean mVisible;
    private float mLastTouchX;
    private float mLastTouchY;
    private int oldWidth;
    private int oldHeight;
    private int mActivePointerId = INVALID_POINTER_ID;
    private VelocityTracker mVelocityTracker = null;
    private float maxScaleFactor = 1.4f;
    private boolean runnable = false;
    private FrameLayout circularView;
    public CountDownTimer thread;
    public boolean canGo = true;
    private SparkButton happy_die;
    private SparkButton sick_die;
    private Typeface roboto;
    private Handler handler;
    private Runnable r;
    private Fragment callFragment;
    private TextView timer;
    private boolean win = false;

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */

    private void resetView(View semicircle) {
        mActivePointerId = INVALID_POINTER_ID;
        semicircle.getLayoutParams().width = oldWidth;
        semicircle.getLayoutParams().height = oldHeight;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_action);
        final RelativeLayout semicircle = (RelativeLayout) findViewById(R.id.semicircle);
        circularView = (FrameLayout) findViewById(R.id.circular);
        ViewTreeObserver vto = semicircle.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                semicircle.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                oldWidth = semicircle.getMeasuredWidth();
                oldHeight = semicircle.getMeasuredHeight();

            }
        });
        roboto = Typeface.createFromAsset(getAssets(),
                "font/Roboto-Thin.ttf");
        //use this.getAssets if you are calling from an Activity
        /* My Items */
        TextView mytxt = (TextView) findViewById(R.id.timer);
        final ImageButton arrowUp = (ImageButton) findViewById(R.id.arrow_up);
        final FrameLayout parent = (FrameLayout) findViewById(R.id.parent);
         timer = (TextView) findViewById(R.id.timer);
        final HTextView sd = (HTextView) findViewById(R.id.textview);
        final String[] sentences = new String[]{"Mi sento male, aiuto!", "Chiama subito i soccorsi!", "Non mi sento molto bene", "Mi serve aiuto"};
        sd.animateText(sentences[new Random().nextInt(sentences.length)]);
        handler = new Handler();
        r = new Runnable() {
            int i = 0;

            public void run() {
                System.out.println(i);
                if (i < 2) {
                    sd.animateText(sentences[new Random().nextInt(sentences.length)]);
                    handler.postDelayed(r, 1000);
                    i++;
                } else {
                    i = 0;
                    handler.removeCallbacks(this);
                    return;
                }

            }
        };

        handler.postDelayed(r, 1300);

        happy_die = (SparkButton) findViewById(R.id.happy_die);
        happy_die.setClickable(false);
        sick_die = (SparkButton) findViewById(R.id.sick_die);
        sick_die.setClickable(false);
        final ObjectAnimator background = (ObjectAnimator) AnimatorInflater.loadAnimator(ActionActivity.this, R.animator.background_color);
        final ObjectAnimator resetPos = (ObjectAnimator) AnimatorInflater.loadAnimator(ActionActivity.this, R.animator.reset_position);
        final TransitionDrawable transition = (TransitionDrawable) semicircle.getBackground();

        background.setInterpolator(new DecelerateInterpolator());
        background.setTarget(findViewById(R.id.arrow_up));

        resetPos.setTarget(semicircle);

        arrowUp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                v.getParent().requestDisallowInterceptTouchEvent(false);
                return false;
            }
        });

        semicircle.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                //scaleFactorLimit = 1;

            }
        });
        semicircle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) v.getLayoutParams();
                int index = event.getActionIndex();
                float x = 0;
                float y = 0;
                if(!canGo) return false;
                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_MOVE:
                        mVelocityTracker.addMovement(event);
                        mVelocityTracker.computeCurrentVelocity(1000);
                        int pointerIndex = event.getPointerId(mActivePointerId);
                        x = event.getX(pointerIndex);
                        y = event.getY(pointerIndex);
                        final float dx = x - mLastTouchX;
                        final float dy = y - mLastTouchY;
                        if (y >= mLastTouchY) break;
                        v.setScaleX(1 + Math.abs(dy) / 1000);
                        v.setScaleY(1 + Math.abs(dy) / 1000);

                        v.setLayoutParams(v.getLayoutParams());
                        break;
                    case MotionEvent.ACTION_DOWN:
                        transition.startTransition(300);
                        background.start();

                        if (mVelocityTracker == null) {
                            // Retrieve a new VelocityTracker object to watch the velocity of a motion.
                            mVelocityTracker = VelocityTracker.obtain();
                        } else {
                            // Reset the velocity tracker back to its initial state.
                            mVelocityTracker.clear();
                        }
                        // Add a user's movement to the tracker.
                        mVelocityTracker.addMovement(event);
                        pointerIndex = MotionEventCompat.getActionIndex(event);
                        x = event.getX(pointerIndex);
                        y = event.getY(pointerIndex);

                        // Remember where we started (for dragging)
                        mLastTouchX = x;
                        mLastTouchY = y;
                        // Save the ID of this pointer (for dragging)
                        mActivePointerId = event.getPointerId(pointerIndex);

                        break;
//
                    case MotionEvent.ACTION_UP:

                        ObjectAnimator scaleX = ObjectAnimator.ofFloat(semicircle, "scaleX", 1);
                        ObjectAnimator scaleY = ObjectAnimator.ofFloat(semicircle, "scaleY", 1);
                        scaleX.setDuration(500);
                        scaleY.setDuration(500);
                        scaleX.setInterpolator(new BounceInterpolator());
                        scaleY.setInterpolator(new BounceInterpolator());
                        AnimatorSet scaleDown = new AnimatorSet();
                        scaleDown.play(scaleX).with(scaleY);
                        makeCall(v);
                        semicircle.setPressed(false);
                        transition.reverseTransition(300);
                        background.reverse();
                        scaleDown.start();
                        break;
                    default:

                        break;

                }
                return true;
            }
        });

        mytxt.setTypeface(roboto);
        countStart();


    }
    private void countStart(){
        thread = new CountDownTimer(((new Random().nextInt(10))+3)*1000, 10) {

            public void onTick(long millisUntilFinished) {
                long dec = millisUntilFinished % 1000;

                timer.setText(((int) millisUntilFinished / 1000) + ":" + dec / 100);
                if(!canGo) thread.cancel();
            }

            public void onFinish() {

                canGo = false;
               int index = getFragmentManager().getBackStackEntryCount() - 1;
                if(index == -1)
                failCycle();


            }
        }.start();
    }
    @Override
    public void fragmentDetached(int i) {
        if (i == 1) {

            win = true;
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            winCycle();
        } else if(!win && !canGo)
            failCycle();
    }

    private void winCycle() {
        sick_die.setChecked(true);
        sick_die.setAnimationSpeed(.8f);
        sick_die.playAnimation();
        sick_die.setEventListener(new SparkEventListener() {
            @Override
            public void onEvent(ImageView button, boolean buttonState) {

            }

            @Override
            public void onEventAnimationEnd(ImageView button, boolean buttonState) {
                winModal();
            }

            @Override
            public void onEventAnimationStart(ImageView button, boolean buttonState) {

            }
        });
        sick_die.setVisibility(View.GONE);
        happy_die.setVisibility(View.VISIBLE);


    }
    private void winModal(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ActionActivity.this, R.style.myDialog));
        LayoutInflater vie = getLayoutInflater();
        dialogBuilder.setCancelable(false);


        View frameError = vie.inflate(R.layout.dialogs, null);
        View success = frameError.findViewById(R.id.dialogSuccess);
        TextView done = (TextView) frameError.findViewById(R.id.msg2);
        done.setTypeface(roboto);
        ((ViewGroup) success.getParent()).removeView(success);
        dialogBuilder.setView(success);
        dialogBuilder.setPositiveButton("Riprova", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                canGo = true;
                countStart();
                handler.postDelayed(r,100);
                win = false;

            }
        });
        dialogBuilder.create();
        dialogBuilder.show();
    }
    private  void failCycle(){
        happy_die.setVisibility(View.GONE);
        sick_die.setVisibility(View.VISIBLE);
        sick_die.setChecked(true);
        sick_die.setAnimationSpeed(.8f);
        sick_die.playAnimation();
        sick_die.setEventListener(new SparkEventListener() {
            @Override
            public void onEvent(ImageView button, boolean buttonState) {

            }

            @Override
            public void onEventAnimationEnd(ImageView button, boolean buttonState) {
                showModal();

            }

            @Override
            public void onEventAnimationStart(ImageView button, boolean buttonState) {

            }
        });
    }
    private void showModal(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ActionActivity.this, R.style.myDialog));
        LayoutInflater vie = getLayoutInflater();
        dialogBuilder.setCancelable(false);


        View frameError = vie.inflate(R.layout.dialogs, null);
        View success = frameError.findViewById(R.id.dialogFail);
        TextView done = (TextView) frameError.findViewById(R.id.msg);
        done.setTypeface(roboto);
        ((ViewGroup) success.getParent()).removeView(success);
        dialogBuilder.setView(success);
        dialogBuilder.setPositiveButton("Riprova", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                canGo = true;
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                countStart();
                handler.postDelayed(r,100);

            }
        });
        dialogBuilder.create();
        dialogBuilder.show();

    }
    private void makeCall(View v){

        if(v.getScaleY() >= maxScaleFactor && circularView.getVisibility() == View.GONE){
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int cx = (int) (v.getPivotX() / 2);
                int cy = (int) (v.getPivotY() / 2);
                int[] locations = new int[2];
                v.getLocationOnScreen(locations);
                locations[0] = (int) v.getX() + v.getWidth() / 2;
                locations[1] = (int) v.getY();
                float finalRadius = (float) Math.hypot(v.getLeft(), v.getTop());
                System.out.println(v.getPivotX() + " " + v.getPivotY() + " " + (float) Math.hypot(v.getPivotX(), v.getPivotY()));
                Animator circular = ViewAnimationUtils.createCircularReveal(circularView, locations[0], locations[1], v.getHeight(), finalRadius);
                circular.setDuration(300);
                circular.setInterpolator(new DecelerateInterpolator());
                circularView.setVisibility(View.VISIBLE);
                circular.start();
                circular.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        makeCallFragment();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
            } else makeCallFragment();
        }
    }
    public void makeCallFragment(){
        Fragment callFragment = new CallFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.circular, (Fragment) callFragment, "Call");
        // Commit the transaction
        transaction.addToBackStack(null);
        transaction.commit();

    }

}
