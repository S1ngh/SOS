package makeitmodena.sos;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by singh on 6/25/17.
 */

public class SelectFragment extends Fragment {
    private FrameLayout panel;
    private MainActivity my;
    private FloatingActionButton goBlock;
    private FloatingActionButton goUnlock;
    private TextView mTextView;
    private  ImageView mImageView;
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_modes, container, false);
        panel = (FrameLayout) view.findViewById(R.id.backPanel);
        mTextView = (TextView) view.findViewById(R.id.infoSimulate);
        goBlock = (FloatingActionButton) view.findViewById(R.id.goblockscreen);
        goUnlock = (FloatingActionButton) view.findViewById(R.id.gounlockscreen);
        final FrameLayout parent = (FrameLayout) view.findViewById(R.id.simulateFragment);

        panel       = (FrameLayout) view.findViewById(R.id.backPanel);
        mTextView   = (TextView) view.findViewById(R.id.infoSimulate);

        my = (MainActivity) getActivity();
        my.blockScreen.setVisibility(View.GONE);
        my.unlockScreen.setVisibility(View.GONE);

//            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                my.myToolbar.setElevation(0);
//            }
        initText();
        goBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBlock();

            }
        });
        goUnlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callUnlock();
            }
        });
        return view;

    }
    public void callBlock(){

        Intent intent = new Intent(getActivity(),BlockActivity.class);

        startActivity(intent);

    }
    public void callUnlock(){
        Intent intent = new Intent(getActivity(), UnlockActivity.class);
        startActivity(intent);
    }

    public void initText(){
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(mTextView,"alpha",0,1);
        anim2.setInterpolator(new DecelerateInterpolator(3));
        ObjectAnimator anim = ObjectAnimator.ofFloat(mTextView,"TranslationY",-50,0);
        AnimatorSet pl = new AnimatorSet();
        pl.playTogether(anim,anim2);
        pl.setDuration(225)
                .setInterpolator(new DecelerateInterpolator());
        pl.start();
    }
    @Override
    public void onResume() {

        super.onResume();
        try {
            getView().setFocusableInTouchMode(true);
            getView().requestFocus();
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    // handle back button

                    getFragmentManager().popBackStack();
                    my.init();
                    my.resetAnimation();
                    return true;

                }

                return false;
            }
        });
    }
    public void animateInit(){
        panel.animate()
                .translationY(0)
                .setInterpolator(new DecelerateInterpolator())
                .setDuration(400)
                .start();
    }
}
