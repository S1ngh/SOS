package makeitmodena.sos;

import android.animation.Animator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WinFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WinFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WinFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private Button goBack;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private MediaPlayer media;
    private CountDownTimer thread;
    private ViewGroup cont;
    GifDrawable gifFromAssets;
    public onFragmentDetached FragmentCommunicator;

    public WinFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters

    @Override
    public void onDetach() {
        super.onDetach();
        thread.cancel();
        cont.setVisibility(View.GONE);

       media.stop();

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
//        FragmentCommunicator = (onFragmentDetached) getActivity();

        final View view= inflater.inflate(R.layout.win_layout, container, false);
        cont = container;
        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
        GifImageView ambulance = (GifImageView) view.findViewById(R.id.ambulance);
        goBack = (Button) view.findViewById(R.id.gobackWin);
        goBack.setVisibility(View.INVISIBLE);
        try {


            gifFromAssets = new GifDrawable(getActivity().getAssets(), "gif/ambulance_scene_low.gif");
            media = new MediaPlayer();
            AssetFileDescriptor descriptor = getActivity().getAssets().openFd("audio/ambulance.mp3");
            media.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(),descriptor.getLength());
            descriptor.close();

            media.prepare();
            media.setVolume(1f,1f);
            media.setLooping(true);
            media.start();

        }catch (IOException e){
            e.printStackTrace();
        }
        ambulance.setBackground(gifFromAssets);
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        thread = new CountDownTimer(2700, 10) {

            public void onTick(long millisUntilFinished) {
                ;
            }

            public void onFinish() {
               showModal();
                goBack.setVisibility(View.VISIBLE);

            }
        }.start();
        return view;

    }
    private  void showModal(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.myDialog));
        LayoutInflater vie = getActivity().getLayoutInflater();
        dialogBuilder.setCancelable(false);


        View frameError = vie.inflate(R.layout.dialogs, null);
        View success = frameError.findViewById(R.id.dialogSuccess);
        TextView done = (TextView) frameError.findViewById(R.id.msg);
        ((ViewGroup) success.getParent()).removeView(success);
        dialogBuilder.setView(success);
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                dialog.cancel();

            }
        });
        dialogBuilder.create();
        dialogBuilder.show();
    }

    public void revealOff(final View v){
        int cx = (int) (v.getMeasuredWidth() / 2);
        int cy = (int) (v.getMeasuredHeight() / 2);

        float finalRadius = (float) Math.hypot(v.getLeft(), v.getTop());
        Animator circular = ViewAnimationUtils.createCircularReveal(v,cx,cy,v.getWidth()*2,0);
        circular.setDuration(600);
        circular.setInterpolator(new DecelerateInterpolator());
        circular.start();
        circular.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                v.setVisibility(View.GONE);
                getActivity().getFragmentManager().popBackStack();

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

    }
}
