package makeitmodena.sos;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.FragmentTransaction;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hanks.htextview.HTextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.TimeZone;

public class UnlockActivity extends AppCompatActivity implements onFragmentDetached {
    private ImageView mImageView;
    private HTextView leftInfo;
    private HTextView rightInfo;
    private TextView date;
    private TextView time;
    private CardView card;
    private CountDownTimer thread;
    private ImageButton phoneCall;
    private LinearLayout parentBlock;
    private FrameLayout parentFragment;
//    private LinearLayout appBar;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_unlock);

        mImageView  = (ImageView)findViewById(R.id.wallpaper);
        time = (TextView) findViewById(R.id.time);
        date = (TextView) findViewById(R.id.date);
        leftInfo = (HTextView) findViewById(R.id.leftCardInfo);
        rightInfo = (HTextView) findViewById(R.id.rightCardInfo);
        card = (CardView) findViewById(R.id.card);
        phoneCall = (ImageButton) findViewById(R.id.callPhone);
        parentBlock = (LinearLayout) findViewById(R.id.appbar);
        parentFragment = (FrameLayout) findViewById(R.id.parentFragment);
        phoneCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentFragment.setVisibility(View.VISIBLE);
                phoneCall.setClickable(false);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.animator.slide_bottom_totop, R.animator.slide_up_tobottom,R.animator.slide_bottom_totop, R.animator.slide_up_tobottom);
                transaction.replace(R.id.parentFragment, new CallFragment(),"Call");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        setAnimation(parentBlock);
        initView();
        initWorkspace();

    }
    private void setAnimation(ViewGroup parent){

        for(int i = 0; i < parent.getChildCount(); i++) {
            if(parent.getChildAt(i) instanceof  ImageButton ){
                if(parent.getChildAt(i).getTag().equals("otherIcon")){
                    final ImageButton temp = (ImageButton) parent.getChildAt(i);
                    temp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AnimationSet shake = (AnimationSet) AnimationUtils.loadAnimation(getApplicationContext(),R.anim.shake);
                            v.startAnimation(shake);
                        }
                    });
                }
            }else if(parent.getChildAt(i) instanceof ViewGroup){
                if(parent.getChildAt(i)  instanceof LinearLayout) { ;
                    LinearLayout view = (LinearLayout) parent.getChildAt(i);
                    setAnimation(view);
                }
            }
        }
    }
    @Override
    public void fragmentDetached(int i){
        thread.cancel();
        phoneCall.setClickable(true);
        initWorkspace();

    }
    public void initView(){
        card.setAlpha(0);
        card.setTranslationY((-1)*100);

    }
    public void initWorkspace(){
        /*I know i know, i have to create a general class to avoid copy paste same code, but i dont have so much time.. and are only 2 activity...*/
        InputStream ims;
        try {
            ims = getAssets().open("images/material1.jpg");
            Drawable d = Drawable.createFromStream(ims, null);
            mImageView.setImageDrawable(d);

        }catch (IOException e){
            e.printStackTrace();
        }
        Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        final int minutes = c.get(Calendar.MINUTE);
        final int hours   = c.get(Calendar.HOUR_OF_DAY);
        final int day     = c.get(Calendar.DAY_OF_MONTH);
        final int month     = c.get(Calendar.MONTH);
        final int year     = c.get(Calendar.YEAR);
        date.setText(day+"-"+month+"-"+year);

        thread = new CountDownTimer(Integer.MAX_VALUE, 1000) {

            public void onTick(long millisUntilFinished) {

                time.setText(hours+":"+minutes);

            }

            public void onFinish() {



            }
        }.start();




    }
    @Override
    protected  void onStart(){
        super.onStart();


    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if(hasFocus){
            card.post(new Runnable() {
                @Override
                public void run() {
                    animate();
                }
            });
        }
    }
    public void animate(){
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(card,"alpha",1);
        anim2.setInterpolator(new DecelerateInterpolator());
        ObjectAnimator anim = ObjectAnimator.ofFloat(card,"TranslationY",0);
        AnimatorSet pl = new AnimatorSet();
        pl.playTogether(anim,anim2);
        pl.setDuration(220);
        pl.setInterpolator(new DecelerateInterpolator());
        pl.start();
        pl.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                card.setAlpha(1);
                rightInfo.animateText("Singh Amarjot");
                leftInfo.animateText("</>");
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }
}
