package makeitmodena.sos;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    public FloatingActionButton simulateButton;
    public FloatingActionButton questionsButton;
    public FloatingActionButton blockScreen;
    public FloatingActionButton unlockScreen;
    private RelativeLayout passingScreenPanel;
    private AnimatorSet flipin;
    private AnimatorSet flipin2;
    private Animator backPanel;
    public Toolbar myToolbar;
    private AnimatorSet play;

    private FrameLayout passingScreen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        questionsButton = (FloatingActionButton) findViewById(R.id.questions);
        simulateButton  = (FloatingActionButton) findViewById(R.id.simulate);
        blockScreen     = (FloatingActionButton) findViewById(R.id.blockscreen);
        unlockScreen    = (FloatingActionButton) findViewById(R.id.unlockscreen);

        passingScreenPanel   = (RelativeLayout) findViewById(R.id.simulatePanel);
        passingScreen   = (FrameLayout) findViewById(R.id.simulateFragmentHolder);


        questionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callQuestionActivity();
            }
        });
        simulateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callActionActivity();
            }
        });
        init();
    }
    public void init(){
        int delta=-500;
        passingScreenPanel.setTranslationY((-1*(700)));
        passingScreenPanel.setAlpha(0);

        blockScreen.setVisibility(View.GONE);
        unlockScreen.setVisibility(View.GONE);
        questionsButton.setVisibility(View.VISIBLE);
        simulateButton.setVisibility(View.VISIBLE);

        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        if(getSupportActionBar() != null) {
            setSupportActionBar(myToolbar);
            getSupportActionBar().setTitle(null);
        }
        myToolbar.setTitle(null);
        myToolbar.setTitleTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorWhite));
        TextView d = (TextView) myToolbar.findViewById(R.id.sosActionBar);

        d.setTypeface(Typeface.createFromAsset(getAssets(),
                "font/canaro_extra_bold.otf"));
        /*I know, when i'll have time i'll make method and reuse code*/


    }
    public void resetAnimation(){
//        simulateButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_call_black_24dp));
//        questionsButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_help_nobackground));

    }
    public void resetAnimation_old(){
        flipin = (AnimatorSet) AnimatorInflater.loadAnimator(this,R.animator.flip_out);
        flipin2 = (AnimatorSet) AnimatorInflater.loadAnimator(this,R.animator.flip_out);
        flipin.setTarget(questionsButton);
        flipin.setDuration(300);
        flipin.setInterpolator(new AccelerateDecelerateInterpolator());
        flipin2.setTarget(simulateButton);
        flipin2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                simulateButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_call_black_24dp));

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        flipin2.setDuration(300);
        flipin.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                questionsButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_help_nobackground));

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        flipin2.setInterpolator(new AccelerateDecelerateInterpolator());
        play = new AnimatorSet();
        play.playTogether(flipin,flipin2);

        play.start();

    }
    private void callQuestionActivity(){
        Intent intent = new Intent(getApplicationContext(),QuestionActivity.class);
        startActivity(intent);
    }
    private void callActionActivity(){
////        questionsButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_phonelink_lock_black_24dp));
//        questionsButton.setVisibility(View.GONE);
////        simulateButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_smartphone_black_24dp));
//        simulateButton.setVisibility(View.GONE);
//        blockScreen.setVisibility(View.VISIBLE);
//        unlockScreen.setVisibility(View.VISIBLE);
//        unlockScreen.invalidate();
//        blockScreen.invalidate();
        passingScreen.setVisibility(View.VISIBLE);
        Fragment selectFragment=  new SelectFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.simulateFragmentHolder,  selectFragment,"SelectButtonsFragment");
        transaction.addToBackStack(null);
        transaction.commit();
//        passingScreenPanel.animate()
//                .translationY(0)
//                .alpha(1)
//                .setDuration(300)
//                .setInterpolator(new DecelerateInterpolator(2))
//                .setListener(new Animator.AnimatorListener() {
//                    @Override
//                    public void onAnimationStart(Animator animation) {
//
//                    }
//
//                    @Override
//                    public void onAnimationEnd(Animator animation) {
//                        questionsButton.setVisibility(View.GONE);
//                        simulateButton.setVisibility(View.GONE);
//                        unlockScreen.setVisibility(View.GONE);
//                        blockScreen.setVisibility(View.GONE);
//                        passingScreen.setVisibility(View.VISIBLE);
//                        Fragment selectFragment=  new SelectFragment();
//                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
//                        transaction.replace(R.id.simulateFragmentHolder,  selectFragment,"SelectButtonsFragment");
//                        transaction.addToBackStack(null);
//
//                        transaction.commit();
//                    }
//
//                    @Override
//                    public void onAnimationCancel(Animator animation) {
//
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animator animation) {
//
//                    }
//                });

    }
    private  void callActionActivity_old(){
        flipin = (AnimatorSet) AnimatorInflater.loadAnimator(this,R.animator.flip_in);
        flipin.setTarget(questionsButton);
        flipin.setDuration(300);
        flipin.setInterpolator(new AccelerateDecelerateInterpolator());
        flipin.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                questionsButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_phonelink_lock_black_24dp));
                questionsButton.setVisibility(View.GONE);
                blockScreen.setVisibility(View.VISIBLE);
                blockScreen.invalidate();
                flipin.removeAllListeners();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        flipin2 = (AnimatorSet) AnimatorInflater.loadAnimator(this,R.animator.flip_in);
        flipin2.setTarget(simulateButton);
        flipin2.setDuration(300);
        flipin2.setInterpolator(new AccelerateDecelerateInterpolator());
        flipin2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                passingScreenPanel.animate()
                        .translationY(0)
                        .alpha(1)
                        .setDuration(300)
                        .setInterpolator(new DecelerateInterpolator(2))
                        .setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                questionsButton.setVisibility(View.GONE);
                                simulateButton.setVisibility(View.GONE);
                                unlockScreen.setVisibility(View.GONE);
                                blockScreen.setVisibility(View.GONE);
                                passingScreen.setVisibility(View.VISIBLE);
                                Fragment selectFragment=  new SelectFragment();
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                transaction.replace(R.id.simulateFragmentHolder,  selectFragment,"SelectButtonsFragment");
                                transaction.addToBackStack(null);

                                transaction.commit();
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                simulateButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_smartphone_black_24dp));
                simulateButton.setVisibility(View.GONE);
                unlockScreen.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        play = new AnimatorSet();
        play.playTogether(flipin,flipin2);
        play.start();


    }
}

