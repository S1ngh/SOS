package makeitmodena.sos;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetFileDescriptor;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.text.Layout;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.io.IOException;
import java.util.Random;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.TimeUnit;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

import static android.content.ContentValues.TAG;

/**
 * A fragment with a Google +1 button.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link CallFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CallFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private TextView[] numPad;
    private String myPattern = "1234567890";
    private EditText showNumber;
    private Typeface roboto;
    private FloatingActionButton backspace;
    private FloatingActionButton call;
    private GifDrawable gifFromAssets;
    private GifImageView gifImage;
    private String myNumber = "118";
    public boolean isCorrect = false;
    private Handler uiHandler;
    private Runnable stopper;
    public onFragmentDetached FragmentCommunicator;
    private  LinearLayout parentKeyboard;
    public boolean failCall = false;
    public  ViewGroup cont;
    BlockActivity myActivity = null;
    private MediaPlayer media;
    UnlockActivity myActivity2 = null;
    public CallFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CallFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CallFragment newInstance(String param1, String param2) {
        CallFragment fragment = new CallFragment();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        numPad = new TextView[10];
    }
    @Override
    public void onDetach() {
        super.onDetach();
        media.stop();
        if(myActivity == null) myActivity2.fragmentDetached(1);
        else myActivity.fragmentDetached(1);

    }
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        ActionActivity a;
//        if(context instanceof ActionActivity){
//            a = (ActionActivity) context;
//            FragmentCommunicator = (onFragmentDetached) a;
//
//        }
//
//    }
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        cont = container;
        final View view = inflater.inflate(R.layout.fragment_call, container, false);
        if(getActivity().getClass() == BlockActivity.class)
             myActivity = (BlockActivity) getActivity();
        else myActivity2 = (UnlockActivity) getActivity();
        final LinearLayout parent = (LinearLayout) view.findViewById(R.id.parentNum);
        roboto =    Typeface.createFromAsset(getActivity().getAssets(),
                "font/Roboto-Thin.ttf");
        showNumber = (EditText) view.findViewById(R.id.text);
        backspace = (FloatingActionButton) view.findViewById(R.id.backspace);
        call = (FloatingActionButton) view.findViewById(R.id.call);
        showNumber.setTypeface(roboto);
        parentKeyboard = (LinearLayout) view.findViewById(R.id.parentKeyboard);
        numPad = new TextView[12];
        for(int i = 0; i < numPad.length;i++){
            numPad[i] = new TextView(getActivity().getApplicationContext());

        }
        setListener((ViewGroup)view);
        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNumber.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL));
            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(showNumber.getText().toString().equals(myNumber)){

                        FrameLayout viewCircular = (FrameLayout) view.findViewById(R.id.callCircular);
                        revealOn(viewCircular);


                }
                else{
                    try {
                        media = new MediaPlayer();
                        AssetFileDescriptor descriptor = getActivity().getAssets().openFd("audio/wrong.mp3");
                        media.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                        descriptor.close();

                        media.prepare();
                        media.setVolume(1f, 1f);
                        media.setLooping(false);
                        media.start();
                    }catch(IOException e){
                        e.printStackTrace();
                    }
                    showModal();
                }
            }
        });



        
        return view;
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    private void showModal(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.myDialog));
        LayoutInflater vie = getActivity().getLayoutInflater();
        dialogBuilder.setCancelable(false);


        View frameError = vie.inflate(R.layout.dialogs, null);
        View success = frameError.findViewById(R.id.dialogFail);
        TextView done = (TextView) frameError.findViewById(R.id.msg);
        done.setTypeface(roboto);
        ((ViewGroup) success.getParent()).removeView(success);
        dialogBuilder.setView(success);
        dialogBuilder.setPositiveButton("Riprova", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {


            }
        });
        dialogBuilder.create();
        dialogBuilder.show();

    }
    public void revealOn(final View v) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int cx = (int) (v.getPivotX() / 2);
            int cy = (int) (v.getPivotY() / 2);
            int[] locations = new int[2];
            call.getLocationOnScreen(locations);
            float finalRadius = (float) Math.hypot(parentKeyboard.getWidth(), parentKeyboard.getHeight());
            System.out.println(finalRadius);
            Animator circular = ViewAnimationUtils.createCircularReveal(v, locations[0], locations[1], 0, finalRadius);
            circular.setDuration(200);
            circular.setInterpolator(new DecelerateInterpolator(.3f));
            v.setVisibility(View.VISIBLE);
            circular.start();
            circular.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    WinFragment winFragment = new WinFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                    fragmentTransaction.replace(R.id.callCircular, winFragment, "Win");
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }else{
            v.setVisibility(View.VISIBLE);
            WinFragment winFragment = new WinFragment();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.replace(R.id.callCircular, winFragment, "Win");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }
    public void revealOff(final View v) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            int cx = (int) (v.getMeasuredWidth() / 2);
            int cy = (int) (v.getMeasuredHeight() / 2);

            float finalRadius = (float) Math.hypot(v.getLeft(), v.getTop());
            System.out.println(v);
            Animator circular = ViewAnimationUtils.createCircularReveal(v, cx, cy, v.getWidth() * 2, 0);
            circular.setDuration(600);
            circular.setInterpolator(new DecelerateInterpolator());
            circular.start();
            circular.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(View.GONE);
                    getActivity().getFragmentManager().popBackStack();

                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        } else{
            v.setVisibility(View.GONE);
            WinFragment winFragment = new WinFragment();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.replace(R.id.callCircular, winFragment, "Win");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        }


    }
    private void goBack(final View v){
        revealOff(v);

    }
    private void setListener(final ViewGroup view){
        numPad[0] = (TextView) view.findViewById(R.id.key0);
        numPad[1] = (TextView) view.findViewById(R.id.key1);
        numPad[2] = (TextView) view.findViewById(R.id.key2);
        numPad[3] = (TextView) view.findViewById(R.id.key3);
        numPad[4] = (TextView) view.findViewById(R.id.key4);
        numPad[5] = (TextView) view.findViewById(R.id.key5);
        numPad[6] = (TextView) view.findViewById(R.id.key6);
        numPad[7] = (TextView) view.findViewById(R.id.key7);
        numPad[8] = (TextView) view.findViewById(R.id.key8);
        numPad[9] = (TextView) view.findViewById(R.id.key9);

//         view.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
//            @Override
//            public boolean onPreDraw () {
//                //We only care the first time it happens, so remove it
//                view.getViewTreeObserver().removeOnPreDrawListener(this);
//
//                int  temp[] = new int[2];
//                int  temp2[] = new int[2];
//                for(int j = 0; j < 10; j++){
//                   int i = new Random().nextInt(10);
//
//                    TextView tempest = numPad[i];
//                    ViewGroup tempesti = (ViewGroup)tempest.getParent();
//                    int indexi = tempesti.indexOfChild(tempest);
//
//                    TextView tempest2 = numPad[j];
//                    ViewGroup tempestj = (ViewGroup)tempest2.getParent();
//                    int indexj = tempesti.indexOfChild(tempest2);
//
//                    tempesti.removeView(tempest);
//                    tempestj.addView(tempest);
//                    tempestj.removeView(tempest2);
//                    tempesti.addView(tempest2);
//
//
//                }
//                //Post your animation here, then return true
//                return true;
//            }
//        });


        for (TextView item: numPad
             ) {
            //item.setTypeface(roboto);
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int keypressedid = v.getId();
                    switch (keypressedid){
                        case R.id.key0:
                            showNumber.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_0));
                            break;
                        case R.id.key1:
                            showNumber.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_1));

                            break;
                        case R.id.key2:
                            showNumber.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_2));

                            break;
                        case R.id.key3:
                            showNumber.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_3));

                            break;
                        case R.id.key4:
                            showNumber.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_4));

                            break;
                        case R.id.key5:
                            showNumber.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_5));

                            break;
                        case R.id.key6:
                            showNumber.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_6));

                            break;
                        case R.id.key7:
                            showNumber.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_7));

                            break;
                        case R.id.key8:
                            showNumber.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_8));

                            break;
                        case R.id.key9:
                            showNumber.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_9));
                            break;
                    }
                }
            });

        }
        /*if(v instanceof ViewGroup) {
            ViewGroup d = (ViewGroup) v;
            setListener(d);
        }
        else{
            System.out.println(v.getTag());
            if(v instanceof TextView){
                TextView number = (TextView) v;
                if((int) (myPattern.charAt(Integer.parseInt(number.getTag().toString()))) == (int) ((String) number.getTag()).charAt(0)) {
                        System.out.println("entered");
                    number.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            System.out.println(v.getTag());
                        }
                    });
                }
            }
        }*/

    }




}
